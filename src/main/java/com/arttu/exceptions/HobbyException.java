package com.arttu.exceptions;

/**
 * Class with custom exceptions for hobby class.
 * */
public class HobbyException extends Exception {

    /**
     * Constructor by default.
     * */
    public HobbyException() {
    }

    /**
     * Constructor with one parameter.
     * @param s is string.
     * */
    public HobbyException(final String s) {
        super(s);
    }

    /**
     * Constructor with 2 params.
     * @param s is string.
     * @param throwable is throwable.
     * */
    public HobbyException(final String s, final Throwable throwable) {
        super(s, throwable);
    }

    /**
     * Constructor with throwable.
     * @param throwable is throwable.
     * */
    public HobbyException(final Throwable throwable) {
        super(throwable);
    }

    /**
     * Constructor with 4 params.
     * @param s is string.
     * @param throwable is throwable.
     * @param b is b.
     * @param b1 is b1.
     * */
    public HobbyException(final String s, final Throwable throwable,
                          final boolean b, final boolean b1) {
        super(s, throwable, b, b1);
    }
}
