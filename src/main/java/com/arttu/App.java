package com.arttu;

import com.arttu.exceptions.HobbyException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

/**
 * Main class consists of two part.
 * First one is representing task one with
 * two parameters: name and hoursInGeneral
 * Second one is representing task two with
 * eight parameters: name, personalId, hoursInGeneral,
 * hoursToday, score, efficiencyCoeficientToday,
 * efficiencyCoeficientInGeneral, isCool
 */
public final class App {

    /**
     * Logger.
     * */
    private static final Logger LOGGER = LogManager.getLogger(App.class);
    /**
     * Number.
     * */
    private static final int NUMBER = 10;

    /**
     * Private constructor.
     * */
    private App() { }

    /**
     * Main method.
     * @param args is args.
     * */
    public static void main(final String[] args) {

        Hobby hobby = new Hobby();
        Hobby hobby1 = new Hobby("Football");
        Hobby hobby2 = new Hobby("Football", NUMBER);

        hobby.setName("Basketball");
        hobby.setHoursInGeneral(NUMBER);
        hobby1.setHoursInGeneral(NUMBER);

        LOGGER.log(Level.INFO, hobby.getName() + " "
                + hobby.getHoursInGeneral());
        LOGGER.log(Level.INFO, hobby1.getName() + " "
                + hobby1.getHoursInGeneral());
        LOGGER.log(Level.INFO, hobby2.getName() + " "
                + hobby2.getHoursInGeneral());

        Scanner scanner = new Scanner(System.in);

        LOGGER.log(Level.ALL, "Enter the name");
        String name = scanner.nextLine();
        LOGGER.log(Level.TRACE, "Enter the personal ID");
        long personalId = scanner.nextLong();
        LOGGER.log(Level.DEBUG, "Enter the hours in general");
        int hoursInGeneral = scanner.nextInt();
        LOGGER.log(Level.INFO, "Enter the hours today");
        short hoursToday = scanner.nextShort();
        LOGGER.log(Level.WARN, "Enter the score");
        byte score = scanner.nextByte();
        LOGGER.log(Level.ERROR, "Enter the efficiencyCoeficientToday");
        float efficiencyCoeficientToday = scanner.nextFloat();
        LOGGER.log(Level.FATAL, "Enter the efficiencyCoeficientInGeneral");
        double efficiencyCoeficientInGeneral = scanner.nextDouble();
        LOGGER.log(Level.ALL, "Do you like your sport? (True or false)");
        boolean isCool = scanner.nextBoolean();

        Hobby taskTwoHobby = new Hobby(personalId, name, hoursInGeneral,
                score, efficiencyCoeficientToday,
                efficiencyCoeficientInGeneral, isCool);

        LOGGER.log(Level.ALL, "Using constructor: " + taskTwoHobby + "\n");

        LOGGER.log(Level.ALL, "Using accessors: "
                + taskTwoHobby.getPersonalId() + " "
                + taskTwoHobby.getName() + " "
                + taskTwoHobby.getHoursInGeneral() + " "
                + taskTwoHobby.getScore() + " "
                + taskTwoHobby.getEfficiencyCoeficientToday() + " "
                + taskTwoHobby.getEfficiencyCoeficientInGeneral()
                + " " + taskTwoHobby.isCool());
        try {
            hobby.printName("NFSsdkl");
        } catch (HobbyException e) {
            LOGGER.error(e);
        }

        hobby2.testPrintName();
    }
}
