package com.arttu;

import com.arttu.exceptions.HobbyException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Class Hobby.
 * */
public class Hobby {

    /**
     * Logger.
     * */
    private static Logger logger = LogManager.getLogger();

    /**
     * Personal id.
     * */
    private long personalId;
    /**
     * Name.
     * */
    private String name;
    /**
     * Hours in general.
     * */
    private int hoursInGeneral;
    /**
     * Score.
     * */
    private byte score;
    /**
     * Coefficient of efficiency today.
     * */
    private float efficiencyCoeficientToday;
    /**
     * Coefficient of efficiency in general.
     * */
    private double efficiencyCoeficientInGeneral;
    /**
     * Is cool.
     * */
    private boolean isCool;

    /**
     * Constructor by default.
     * */
    public Hobby() {
        logger.log(Level.TRACE, "Empty constructor");
    }

    /**
     * Constructor.
     * @param nam is name.
     * */
    public Hobby(final String nam) {
        logger.log(Level.INFO, "Constructor with one parameter");
        this.name = nam;
    }

    /**
     * Constructor.
     * @param nam is name.
     * @param hoursInGenera is hours in general.
     * */
    public Hobby(final String nam, final int hoursInGenera) {
        logger.log(Level.ALL, "Constructo with two parameters");
        this.name = nam;
        this.hoursInGeneral = hoursInGenera;
    }

    /**
     * Constructor.
     * @param nam is name.
     * @param hoursInGenera is hours in general.
     * @param personalI is personal id.
     * @param scor is score.
     * @param efficiencyCoeficientInGenera Coefficient of
     *                                     efficiency in general.
     * @param efficiencyCoeficientToda Coefficient of efficiency today.
     * @param isCoo is cool.
     * */
    public Hobby(final long personalI, final String nam,
                 final int hoursInGenera, final byte scor,
                 final float efficiencyCoeficientToda,
                 final double efficiencyCoeficientInGenera,
                 final boolean isCoo) {
        this.personalId = personalI;
        this.name = nam;
        this.hoursInGeneral = hoursInGenera;
        this.score = scor;
        this.efficiencyCoeficientToday = efficiencyCoeficientToda;
        this.efficiencyCoeficientInGeneral = efficiencyCoeficientInGenera;
        this.isCool = isCoo;
    }

    /**
     * Accessor of personal id field.
     * @return personalId.
     * */
    public final long getPersonalId() {
        return personalId;
    }

    /**
     * Mutator of personal id field.
     * @param personalI is personal id.
     * */
    public final void setPersonalId(final long personalI) {
        this.personalId = personalI;
    }

    /**
     * Accessor of personal name field.
     * @return name.
     * */
    public final String getName() {
        return name;
    }

    /**
     * Mutator of name field.
     * @param nam is name.
     * */
    public final void setName(final String nam) {
        this.name = nam;
    }

    /**
     * Accessor of hoursInGeneral field.
     * @return hoursInGeneral.
     * */
    public final int getHoursInGeneral() {
        return hoursInGeneral;
    }

    /**
     * Mutator of hoursInGeneral field.
     * @param hoursInGenera is hours in general.
     * */
    public final void setHoursInGeneral(final int hoursInGenera) {
        this.hoursInGeneral = hoursInGenera;
    }

    /**
     * Accessor of score field.
     * @return score.
     * */
    public final byte getScore() {
        return score;
    }

    /**
     * Mutator of score field.
     * @param scor is score.
     * */
    public final void setScore(final byte scor) {
        this.score = scor;
    }

    /**
     * Accessor of efficiencyCoeficientToday field.
     * @return efficiencyCoeficientToday.
     * */
    public final float getEfficiencyCoeficientToday() {
        return efficiencyCoeficientToday;
    }

    /**
     * Mutator of efficiencyCoeficientToday field.
     * @param efficiencyCoeficientToda is personal id.
     * */
    public final void setEfficiencyCoeficientToday(
            final float efficiencyCoeficientToda) {
        this.efficiencyCoeficientToday = efficiencyCoeficientToda;
    }

    /**
     * Accessor of efficiencyCoeficientInGeneral field.
     * @return efficiencyCoeficientInGeneral.
     * */
    public final double getEfficiencyCoeficientInGeneral() {
        return efficiencyCoeficientInGeneral;
    }

    /**
     * Mutator of efficiencyCoeficientInGeneral field.
     * @param efficiencyCoeficientInGenera is personal id.
     * */
    public final void setEfficiencyCoeficientInGeneral(
            final double efficiencyCoeficientInGenera) {
        this.efficiencyCoeficientInGeneral = efficiencyCoeficientInGenera;
    }

    /**
     * Accessor of isCool field.
     * @return isCool.
     * */
    public final boolean isCool() {
        return isCool;
    }

    /**
     * Mutator of is cool field.
     * @param coo is cool.
     */
    public final void setCool(final boolean coo) {
        isCool = coo;
    }

    @Override
    public final String toString() {
        return "Hobby{"
                + "personalId=" + personalId
                + ", name='" + name + '\''
                + ", hoursInGeneral=" + hoursInGeneral
                + ", score=" + score
                + ", efficiencyCoeficientToday=" + efficiencyCoeficientToday
                + ", efficiencyCoeficientInGeneral="
                + efficiencyCoeficientInGeneral
                + ", isCool=" + isCool
                + '}';
    }

    /**
     * Method to test exception.
     * @param naming is naming.
     * @throws HobbyException hobby exception
     * */
    public final void printName(final String naming) throws HobbyException {
        if (naming != null) {
            System.out.println(naming);
        } else {
            throw new HobbyException();
        }
    }

    /**
     * Method to test printName method.
     * */
    public final void testPrintName() {
        try {
            printName("sdnjfks");
        } catch (HobbyException e) {
            System.out.println(e);
        }
    }
}
